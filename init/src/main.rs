use aws_config::load_from_env;
use aws_sdk_dynamodb::{Client, model::AttributeValue};
use lambda_runtime::{Error as LambdaError, LambdaEvent, service_fn};
use serde::{Deserialize, Serialize};
use serde_json::{json, Value};
use simple_logger::SimpleLogger;



#[derive(Debug, Deserialize, Serialize)]
struct Person {
    first_name: String,
    height: f64,
    weight: f64,
}

#[derive(Deserialize)]
struct Request {
    customer_id: String,
}

#[derive(Debug, Serialize)]
struct Response {
    average_bmi: f64,
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error + Send + Sync + 'static>> {
    let func = handler_fn(lambda_handler);
    lambda_runtime::run(func).await?;
    Ok(())
}

async fn handler(event: LambdaEvent<Request>) -> Result<Value, LambdaError> {
    let request = event.payload;
    let config = load_from_env().await;
    let client = Client::new(&config);

    let response = calculate_average_bmi(&client, request.customer_id).await;

    Ok(json!({ "BMI": response.average }))
}

// Function to calculate the average BMI
async fn calculate_average_bmi(client: &Client, customer_id: String) -> f64 {
    let table_name = "BMI_tables";

    let resp = client.get_item()
        .table_name(table_name)
        .key("customer_id", AttributeValue::S(&customer_id))
        .send()
        .await;

    let item = resp.item.unwrap();
    let height = item.get("height").unwrap().n.unwrap().parse::<f64>().unwrap();
    let weight = item.get("weight").unwrap().n.unwrap().parse::<f64>().unwrap();

    let bmi = weight / (height * height);
    bmi

}
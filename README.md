# rust_serverless_microservice
In this project, I will create a serverless microservice using Rust and AWS Lambda.


## Description
Created a main.rs file that contains the code for the serverless microservice. The code is written in Rust and uses the AWS Lambda runtime for Rust. The microservice will accept a JSON payload and return a JSON response. The microservice will be deployed to AWS Lambda using the AWS CLI. We will create a micriservice that will accept a JSON payload and return a JSON response. The microservice will be deployed to AWS Lambda using the AWS CLI. A table will be created in DynamoDB to store the data, containing person's name, height, and weight. See the following example of the JSON payload that the microservice will accept:

```json
{
  "name": "John Doe",
  "height": 180,
  "weight": 80
}
```

The main.rs file will contain the following functions:

- `create_person`: This function will accept a JSON payload and store the data in a DynamoDB table.

- `get_person`: This function will accept a JSON payload and return the data from the DynamoDB table.

- `update_person`: This function will accept a JSON payload and update the data in the DynamoDB table.

- `delete_person`: This function will accept a JSON payload and delete the data from the DynamoDB table.

The microservice will be deployed to AWS Lambda using the AWS CLI. The following steps will be followed:

- Create a new Rust project using Cargo.

- Add the AWS Lambda runtime for Rust as a dependency.

- Write the code for the microservice in the main.rs file.

- Build the microservice using Cargo.

- Deploy the microservice to AWS Lambda using the AWS CLI.

- Test the microservice using the AWS CLI.

Here is the code for the main.rs file:

```rust
use lambda_http::{handler, lambda, Context, Request, Response};
use serde_json::{json, Value};
use std::error::Error;

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    lambda!(handler);
    Ok(())
}

async fn handler(request: Request, _: Context) -> Result<Response, Error> {
    let body = request.body();
    let data: Value = serde_json::from_slice(body.as_ref())?;
    let operation = data["operation"].as_str().unwrap_or("");

    match operation {
        "create" => create_person(data).await,
        "get" => get_person(data).await,
        "update" => update_person(data).await,
        "delete" => delete_person(data).await,
        _ => Ok(Response::builder()
            .status(400)
            .body("Invalid operation".into())
            .expect("Failed to render response")),
    }
}
```


## Screenshot
![Alt text](./img/lambda_fn.png)

![Alt text](./img/dynamodb.png)